![SQL Murder Mystery](https://mystery.knightlab.com/174092-clue-illustration.png)

# SQL Murder Mystery

> Un meurtre à SQL City !

Un meurtre à eu lieu à SQL City ! L'activité _SQL Murder Mystery_ est conçue
pour être une leçon autonome pour apprendre les concepts du langage SQL
et un jeu ludique pour les programmers SQL avec un peu d'expérience pour
résoudre un crime intriguant !

## Objectifs

Le jeu _SQL Murder Mystery_ fait appel à vos connaissances actuelles du langage SQL.
Vous devrez faire preuve de logique et devrez comprendre quel concept SQL utiliser
pour trouver de nouveaux indices (sélection de colonnes, filtrage, groupements, jointures, etc.)

L'action se passe sur [mystery.knightlab.com](https://mystery.knightlab.com) :

- Résoudre le meutre de SQL City en interrogeant la base de données en ligne
- Créer une branche par groupe et compléter la section [Investigation](#investigation)
- Prendre note de **toutes les requêtes SQL nécessaires** et de leur résultat
- Décrire le cheminement logique pour y arriver

**Astuces avec Markdown** :

- Vous pouvez écrire des blocs de code SQL, par exemple :

```sql
SELECT *
FROM ma_table
WHERE col_1 > 0;
```

- Vous pouvez citer des résultats, par exemple :

> J'ai aperçu 2 suspects par la fenêtre de ma chambre !

- Vous pouvez noter des résultats sous forme de table, par exemple :

| id  | prenom | nom   |
| --- | ------ | ----- |
| 1   | John   | Doe   |
| 2   | Jane   | Doe   |
| 3   | James  | Smith |

## Modalités

- Activité en groupe de 3 à 4 apprenants
- Chaque apprenant doit savoir construire et exécuter toutes les requêtes SQL élaborées

## Investigation

`[A COMPLETER]`
